/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colecciones;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 *
 * @author Duoc
 */
public class Colecciones {

    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       /* ArrayList valores = new ArrayList();
        valores.add("Hola");
        valores.add(1);
        valores.add(new Persona("Juan", "1-9"));
        valores.add(1,"Inicio");
        System.out.println(valores);
        
        valores.remove("Inicio");
        System.out.println(valores);
        String saludo = (String)valores.get(0);
        
        valores.clear();
        System.out.println(valores);
        //Persona p = (Persona)valores.get(3);
        
        //System.out.println("Persona: " + p);
        
        for(Object aux : valores){
            System.out.println(aux);
        }*/
        
        ArrayList<String> valoresString = new ArrayList<String>();
        valoresString.add("Uno");
        valoresString.add("Dos");
        valoresString.add("Tres");
        valoresString.add("Cuatro");
        valoresString.add("Cinco");
        
        System.out.println(valoresString);
        for(String aux : valoresString){
            System.out.println(aux);
        }
        
        Collections.sort(valoresString);
        
        System.out.println(valoresString);
        for(String aux : valoresString){
            System.out.println(aux);
        }
        
        /*
        ArrayList<Persona> valoresPersona = new ArrayList<Persona>();
        Persona persona1 = new Persona("Juan", "1-9");
        Persona persona2 = new Persona("Bort", "2-7");
        Persona persona3 = new Persona("Goku", "3-5");
        valoresPersona.add(persona1);
        valoresPersona.add(persona2);
        valoresPersona.add(persona3);
        valoresPersona.add(new Persona("Persona4", "4-1"));
        System.out.println(valoresPersona);
        System.out.println("Cantidad de personas: " + valoresPersona.size());
        for(int x = 0; x < valoresPersona.size() ; x++){
            System.out.println(x);
        }
        
        for(int x = valoresPersona.size()-1; x >= 0; x--){
            System.out.println(x);
        }*/
   }
    
}
